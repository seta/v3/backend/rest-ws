# pylint: disable=missing-function-docstring

from flask_sqlalchemy import SQLAlchemy
from interface import implements
from injector import inject

from seta_flask_server.repository.interfaces import IDbConfig, IStoredSearchesBroker
from seta_flask_server.repository.models import StoredSearchItem, SearchItemType

from seta_flask_server.repository.orm_models import StoredSearchItemOrm


class OrmStoredSearchesBroker(implements(IStoredSearchesBroker)):
    @inject
    def __init__(self, config: IDbConfig) -> None:
        self.config = config
        self.db: SQLAlchemy = config.get_db()

    def create(self, item: StoredSearchItem):
        self.db.session.add(_to_orm_model(item))
        self.db.session.commit()

    def get_all_by_parent(self, user_id: str, parent_id: str) -> list[StoredSearchItem]:
        items = (
            self.db.session.query(StoredSearchItemOrm)
            .filter_by(user_id=user_id, parent_id=parent_id)
            .order_by(StoredSearchItemOrm.item_type, StoredSearchItemOrm.name)
            .all()
        )

        if items:
            return [_from_orm_model(item) for item in items]

        return []

    def get_by_id(self, user_id: str, item_id: str) -> StoredSearchItem:
        item = (
            self.db.session.query(StoredSearchItemOrm)
            .filter_by(user_id=user_id, item_id=item_id)
            .first()
        )

        if item is None:
            return None

        return _from_orm_model(item)

    def update(self, item: StoredSearchItem):
        orm_item = (
            self.db.session.query(StoredSearchItemOrm)
            .filter_by(user_id=item.user_id, item_id=item.id)
            .first()
        )

        if item.name:
            orm_item.name = item.name
        orm_item.parent_id = item.parent_id

        if item.search and item.type == SearchItemType.Document:
            orm_item.search = item.search

        orm_item.modified_at = item.modified_at

        self.db.session.commit()

    def delete(self, user_id: str, item_id: str):
        _ids = []

        self._construct_cascade_ids(user_id=user_id, parent_id=item_id, _ids=_ids)

        self.db.session.query(StoredSearchItemOrm).filter(
            StoredSearchItemOrm.user_id == user_id,
            StoredSearchItemOrm.item_id.in_(_ids),
        ).delete(synchronize_session=False)

        self.db.session.commit()

    def _construct_cascade_ids(self, user_id: str, parent_id: str, _ids: list[str]):
        children = (
            self.db.session.query(StoredSearchItemOrm.item_id)
            .filter_by(user_id=user_id, parent_id=parent_id)
            .all()
        )

        for item in children:
            self._construct_cascade_ids(
                user_id=user_id, parent_id=item.item_id, _ids=_ids
            )

        _ids.append(parent_id)


def _from_orm_model(item: StoredSearchItemOrm) -> StoredSearchItem:
    return StoredSearchItem(
        user_id=item.user_id,
        id=item.item_id,
        name=item.name,
        parent_id=item.parent_id,
        type=item.item_type,
        search=item.search,
        created_at=item.created_at,
        modified_at=item.modified_at,
    )


def _to_orm_model(item: StoredSearchItem) -> StoredSearchItemOrm:
    return StoredSearchItemOrm(
        user_id=item.user_id,
        item_id=item.id,
        name=item.name,
        parent_id=item.parent_id,
        item_type=item.type,
        search=item.search,
        created_at=item.created_at,
        modified_at=item.modified_at,
    )
