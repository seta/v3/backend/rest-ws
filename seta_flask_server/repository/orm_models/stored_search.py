from seta_flask_server.infrastructure.extensions import db


class StoredSearchItemOrm(db.Model):

    __tablename__ = "stored_searches"

    id = db.Column(db.Integer, primary_key=True, autoincrement=True)
    user_id = db.Column(db.String, db.ForeignKey("users.user_id"), nullable=False)
    item_id = db.Column(db.String, nullable=False)
    name = db.Column(db.String, nullable=False)
    parent_id = db.Column(db.String, nullable=True)
    item_type = db.Column(db.Integer, nullable=False)
    search = db.Column(db.JSON, nullable=True)
    created_at = db.Column(db.DateTime, nullable=True)
    modified_at = db.Column(db.DateTime, nullable=True)

    def __repr__(self) -> str:
        return (
            f"<StoredSearchItemOrm {self.id} {self.user_id} {self.item_id} {self.name} "
            f"{self.parent_id} {self.item_type}"
        )
