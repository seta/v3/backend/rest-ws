# pylint: disable=missing-function-docstring
from dataclasses import dataclass

from enum import Enum


class DiscoveryDomainEnum(str, Enum):
    SEARCH = "search"
    RAG = "rag"
    NLP = "nlp"


@dataclass
class CatalogueDiscoveryDomain:
    code: str
    description: str

    @classmethod
    def from_db_json(cls, json_dict: dict):
        return cls(code=json_dict["code"], description=json_dict.get("description"))
