from flask import current_app
from seta_flask_server.infrastructure import extensions


def clear_all_sources():
    """
    Clears a namespace
    :param namespace: str, namespace i.e your:prefix
    """

    prefix = current_app.config["SEARCH_CACHE_PREFIX"]
    namespace = f"{prefix}:sources"

    try:
        lua = f"for i, name in ipairs(redis.call('KEYS', '{namespace}:*')) do redis.call('DEL', name); end"
        return extensions.redis_client.eval(lua, numkeys=0)
    except Exception:
        pass


def clear_user_sources(user_id: str):
    """
    Clears a user cache for data source permissions
    :param namespace: str, namespace i.e your:prefix
    :param user_id: str, user id
    """

    prefix = current_app.config["SEARCH_CACHE_PREFIX"]
    namespace = f"{prefix}:sources"

    try:
        key = f"{namespace}:{user_id}"
        extensions.redis_client.delete(key)
    except Exception as e:
        current_app.logger.exception(e)
