from http import HTTPStatus
import time
from urllib.parse import urljoin
from injector import inject

from flask import Blueprint, abort
from flask import current_app as app, session
from flask import redirect, request, url_for

from seta_flask_server.infrastructure.auth_helpers import (
    create_login_response,
    validate_next_url,
)
from seta_flask_server.repository.interfaces import IUsersBroker, ISessionsBroker
from seta_flask_server.repository.models import SetaUserExt


auth_ecas = Blueprint("auth_ecas", __name__)

MAX_TICKET_VALIDATION_TRIES = 5
SESSION_TICKET_VALIDATION_TRIES = "ticket_validation_tries"


@auth_ecas.route("/login/", methods=["GET"])
@auth_ecas.route("/login/ecas", methods=["GET"])
def login():
    """
    Redirects to ECAS authentication page
    """

    next_url = request.args.get("redirect")

    cas_login_url = _redirect_to_cas_login(next_url)

    app.logger.debug("CAS login URL: %s", cas_login_url)

    return redirect(cas_login_url)


@auth_ecas.route("/login/callback/ecas", methods=["GET"])
@inject
def login_callback_ecas(user_broker: IUsersBroker, session_broker: ISessionsBroker):
    """Callback after ECAS successful authentication"""

    next_url = request.args.get("next")
    ticket = request.args.get("ticket")

    ticket_validation_success = False

    try:
        user, attributes, _ = app.cas_client.verify_ticket(ticket)

        if user:
            ticket_validation_success = True

    except Exception as e:
        app.logger.exception(e)

    if not ticket_validation_success:
        tries = session.get(SESSION_TICKET_VALIDATION_TRIES, 0)

        app.logger.warning(
            "Failed to verify ticket. Try %d of %d",
            tries + 1,
            MAX_TICKET_VALIDATION_TRIES,
        )

        if tries < MAX_TICKET_VALIDATION_TRIES:
            session[SESSION_TICKET_VALIDATION_TRIES] = tries + 1

            cas_login_url = _redirect_to_cas_login(next_url)
            time.sleep(1)  # wait a bit before redirecting
            return redirect(cas_login_url)

        abort(HTTPStatus.UNAUTHORIZED, "Failed to verify ticket.")

    session.pop(SESSION_TICKET_VALIDATION_TRIES, None)

    if not attributes and not attributes.get("email"):
        abort(HTTPStatus.UNAUTHORIZED, "No email attribute received from ECAS.")

    # Login successful, redirect according to `next` query parameter.
    admins = app.config.get("ROOT_USERS", [])
    email = str(attributes["email"]).lower()
    attributes["is_admin"] = email in admins

    seta_user = SetaUserExt.from_ecas_json(attributes)

    if not next_url or not validate_next_url(next_url):
        next_url = app.home_route

    next_url = urljoin(next_url, "?action=login")

    auth_user = user_broker.authenticate_user(seta_user)

    if auth_user is None:
        #! user is not active
        return redirect(app.home_route)

    response = create_login_response(
        seta_user=auth_user, session_broker=session_broker, next_url=next_url
    )

    return response


@auth_ecas.route("/logout/ecas")
def logout_ecas():
    """Redirect to ECAS logout page"""

    redirect_url = url_for("auth._seta_logout_callback", _external=True)
    cas_logout_url = app.cas_client.get_logout_url(redirect_url)

    app.logger.debug("CAS logout URL: %s", cas_logout_url)

    return redirect(cas_logout_url)


def _redirect_to_cas_login(next_url: str) -> str:

    app.cas_client.service_url = url_for(
        "auth_ecas.login_callback_ecas", next=next_url, _external=True
    )

    return app.cas_client.get_login_url()
