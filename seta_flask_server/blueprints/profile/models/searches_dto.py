from flask_restx import Model, fields
from seta_flask_server.infrastructure import helpers
from seta_flask_server.infrastructure.dto import response_dto

search_model = Model(
    "SearchItem",
    {
        "type": fields.Integer(description="Item type; 0 - Folder, 1 - Item"),
        "parentId": helpers.NullableString(description="Parent identifier"),
        "name": fields.String(description="Name"),
        "search": fields.Raw(description="Stored search"),
    },
)

stored_search_model = Model(
    "StoredSearchItem",
    {
        "id": fields.String(description="Search identifier"),
        "type": fields.Integer(description="Item type; 0 - Folder, 1 - Item"),
        "parentId": helpers.NullableString(description="Parent identifier"),
        "name": fields.String(description="Name"),
        "search": fields.Raw(description="Stored search"),
        "path": fields.List(fields.String, description="Path array of names"),
    },
)

stored_search_model["children"] = fields.List(
    fields.Nested(model=stored_search_model, skip_none=True), description="Child items"
)

search_items = Model(
    "SearchItems",
    {"items": fields.List(fields.Nested(model=stored_search_model, skip_none=True))},
)

ns_models = {
    search_model.name: search_model,
    stored_search_model.name: stored_search_model,
    search_items.name: search_items,
    response_dto.error_fields_model.name: response_dto.error_fields_model,
    response_dto.error_model.name: response_dto.error_model,
    response_dto.response_message_model.name: response_dto.response_message_model,
}
