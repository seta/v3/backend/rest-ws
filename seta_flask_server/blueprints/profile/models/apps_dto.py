from flask_restx import Model, fields

from seta_flask_server.infrastructure.constants import UserStatusConstants

app_model = Model(
    "AppInfo",
    {
        "user_id": fields.String(description="User Identifier"),
        "name": fields.String(description="Application name", attribute="app_name"),
        "description": fields.String(
            description="Application description", attribute="app_description"
        ),
        "status": fields.String(
            "Status", enum=[UserStatusConstants.Active, UserStatusConstants.Disabled]
        ),
        "provider": fields.String(
            description="Authentication provider to use for getting access token"
        ),
    },
)

new_app_model = Model(
    "NewApplication",
    {
        "name": fields.String(description="Application name", required=True),
        "description": fields.String(
            description="Application description", required=True
        ),
        "copyPublicKey": fields.Boolean(
            description="Copy the public key from parent to the new application?",
            default=False,
        ),
    },
)

update_app_model = Model(
    "UpdateApplication",
    {
        "new_name": fields.String(description="Updated application name"),
        "description": fields.String(
            description="Application description", required=True
        ),
        "status": fields.String(
            "Status",
            required=False,
            enum=[UserStatusConstants.Active, UserStatusConstants.Disabled],
        ),
    },
)

ns_models = {
    app_model.name: app_model,
    new_app_model.name: new_app_model,
    update_app_model.name: update_app_model,
}
