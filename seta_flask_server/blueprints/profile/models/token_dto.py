from flask_restx import Model, fields
from seta_flask_server.infrastructure.dto import response_dto

access_token = Model(
    "AccessTokken",
    {"accessToken": fields.String(description="Access token")},
)

expires_at = Model(
    "ExpiresDate",
    {
        "expires": fields.Date(
            description="Expiration date of the access token. Format: YYYY-MM-DD",
            required=True,
        )
    },
)

ns_models = {
    access_token.name: access_token,
    expires_at.name: expires_at,
    response_dto.error_fields_model.name: response_dto.error_fields_model,
    response_dto.error_model.name: response_dto.error_model,
    response_dto.response_message_model.name: response_dto.response_message_model,
}
