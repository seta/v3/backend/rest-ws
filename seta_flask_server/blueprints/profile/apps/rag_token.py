from http import HTTPStatus
from injector import inject

from flask import current_app

from flask_jwt_extended import jwt_required, get_jwt_identity
from flask_restx import Resource, abort

from seta_flask_server.repository import interfaces
from seta_flask_server.blueprints.profile.models import token_dto as dto
from seta_flask_server.infrastructure.auth_helpers import (
    create_rag_token,
    compute_expire_date,
)

from .ns import applications_ns


@applications_ns.route(
    "/<string:name>/rag-tokens", endpoint="app_rag_token", methods=["POST"]
)
@applications_ns.param("name", "Application name")
class ApplicationRAGTokenResource(Resource):

    @inject
    def __init__(
        self,
        apps_broker: interfaces.IAppsBroker,
        users_broker: interfaces.IUsersBroker,
        *args,
        api=None,
        **kwargs
    ):
        super().__init__(api, *args, **kwargs)

        self.apps_broker = apps_broker
        self.users_broker = users_broker

    @applications_ns.doc(
        description="Generates access token for RAG api with an expiration date of  __maximum 180 days__ in the future.",
        responses={
            int(HTTPStatus.OK): "Public key saved.",
            int(
                HTTPStatus.BAD_REQUEST
            ): "Expires date is mising, invalid format or out of range.",
        },
        security="CSRF",
    )
    @applications_ns.response(int(HTTPStatus.OK), "", dto.access_token)
    @applications_ns.response(
        int(HTTPStatus.BAD_REQUEST), "", dto.response_dto.error_model
    )
    @applications_ns.expect(dto.expires_at, validate=True)
    @jwt_required(fresh=True)
    def post(self, name: str):
        """Generates access token for the application."""

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        app = self.apps_broker.get_by_parent_and_name(parent_id=user_id, name=name)

        if app is None:
            abort(HTTPStatus.NOT_FOUND, "Application not found")

        user = self.users_broker.get_user_by_id(app.user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        try:
            expires_str = applications_ns.payload["expires"]
            expires = compute_expire_date(expires_str)

            secret = current_app.config["RAG_SECRET_KEY"]

            access_token = create_rag_token(user, secret, expires)

            return {"accessToken": access_token}

        except ValueError as ve:
            abort(HTTPStatus.BAD_REQUEST, str(ve))

        return None
