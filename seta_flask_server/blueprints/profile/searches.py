from http import HTTPStatus
from injector import inject

from flask import jsonify, current_app
from flask_jwt_extended import get_jwt_identity, jwt_required
from flask_restx import Namespace, Resource, abort

from werkzeug.exceptions import HTTPException

from seta_flask_server.repository import interfaces
from .models import searches_dto as dto
from .logic import searches_logic

searches_ns = Namespace(
    "Searches", validate=True, description="SETA User Stored Searches"
)

searches_ns.models.update(dto.ns_models)


@searches_ns.route("", endpoint="search_tree", methods=["GET", "POST"])
class SearchItemsResource(Resource):
    """Get a list of search items of the authorized user and expose POST for new item."""

    @inject
    def __init__(
        self,
        searches_broker: interfaces.IStoredSearchesBroker,
        users_broker: interfaces.IUsersBroker,
        *args,
        api=None,
        **kwargs,
    ):
        super().__init__(api, *args, **kwargs)

        self.searches_broker = searches_broker
        self.users_broker = users_broker

    @searches_ns.doc(
        description="Retrieve searches tree for this user.",
        responses={int(HTTPStatus.OK): "Retrieved tree."},
        security="CSRF",
    )
    @searches_ns.response(int(HTTPStatus.OK), "Success", dto.search_items)
    @jwt_required()
    def get(self):
        """Retrieve user searches tree"""

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        try:
            items = searches_logic.get_tree(
                user_id=user_id, broker=self.searches_broker
            )
        except Exception:
            current_app.logger.exception("SearchItemsResource->get")
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        return jsonify(items)

    @searches_ns.doc(
        description="Create search item.",
        responses={
            int(HTTPStatus.CREATED): "Items added.",
            int(HTTPStatus.BAD_REQUEST): "Errors in request payload",
        },
        security="CSRF",
    )
    @searches_ns.expect(dto.search_model)
    @searches_ns.response(int(HTTPStatus.CREATED), "Success", dto.search_model)
    @searches_ns.response(int(HTTPStatus.BAD_REQUEST), "", dto.response_dto.error_model)
    @jwt_required()
    def post(self):
        """Create search item"""

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        data = searches_ns.payload
        current_app.logger.debug(data)

        try:
            item = searches_logic.parse_args_new_items(user_id, data)

            self.searches_broker.create(item)

            response = jsonify(item.to_json_api())
            response.status_code = HTTPStatus.CREATED

            return response
        except HTTPException:
            raise
        except Exception:
            current_app.logger.exception("SearchItemsResource->post")
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)


@searches_ns.route(
    "/<string:item_id>", endpoint="search_item", methods=["PUT", "DELETE"]
)
@searches_ns.param("item_id", "Item identifier")
class SearchItemResource(Resource):
    """Handles HTTP requests to URL: /me/searches/{id}."""

    @inject
    def __init__(
        self,
        searches_broker: interfaces.IStoredSearchesBroker,
        users_broker: interfaces.IUsersBroker,
        *args,
        api=None,
        **kwargs,
    ):
        self.searches_broker = searches_broker
        self.users_broker = users_broker

        super().__init__(api, *args, **kwargs)

    @searches_ns.doc(
        description="Update an item in library",
        responses={
            int(HTTPStatus.OK): "Item updated.",
            int(HTTPStatus.NOT_FOUND): "Item not found.",
            int(HTTPStatus.BAD_REQUEST): "Errors in request payload",
        },
        security="CSRF",
    )
    @searches_ns.expect(dto.search_model)
    @jwt_required()
    def put(self, item_id: str):
        """Updates a search item, available to library owner."""

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        args = searches_ns.payload

        item = self.searches_broker.get_by_id(user_id=user_id, item_id=item_id)
        if item is None:
            abort(HTTPStatus.NOT_FOUND, "Item identifier not found")

        try:
            searches_logic.parse_args_update_item(item, args)

            self.searches_broker.update(item)
        except HTTPException:
            raise
        except Exception:
            current_app.logger.exception("SearchItemResource->put")
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        return jsonify(status="success", message="Search item updated")

    @searches_ns.doc(
        description="Remove item from stored searches",
        responses={
            int(HTTPStatus.OK): "Item removed.",
            int(HTTPStatus.NOT_FOUND): "Membership not found.",
        },
        security="CSRF",
    )
    @jwt_required()
    def delete(self, item_id: str):
        """
        Removes an item, available to library owner.
        """

        identity = get_jwt_identity()
        user_id = identity["user_id"]

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)
        if user is None or user.is_not_active():
            abort(HTTPStatus.FORBIDDEN, "Insufficient rights.")

        item = self.searches_broker.get_by_id(user_id=user_id, item_id=item_id)
        if item is None:
            abort(HTTPStatus.NOT_FOUND, "Item identifier not found")

        try:
            self.searches_broker.delete(user_id=user_id, item_id=item_id)
        except Exception:
            current_app.logger.exception("LibraryItemResource->delete")
            abort(HTTPStatus.INTERNAL_SERVER_ERROR)

        return jsonify(status="success", message="Item deleted")
