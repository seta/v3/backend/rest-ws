from flask_restx import Model, fields

discovery_domain_model = Model(
    "DiscoveryDomain",
    {
        "code": fields.String(description="Domain code"),
        "description": fields.String(description="Data source domain description"),
    },
)

discovery_domains_model = Model(
    "DiscoveryDomainsCatalogue",
    {
        "domains": fields.List(
            fields.Nested(model=discovery_domain_model),
            description="List of the discovery domains",
        )
    },
)

ns_models = {
    discovery_domain_model.name: discovery_domain_model,
    discovery_domains_model.name: discovery_domains_model,
}
