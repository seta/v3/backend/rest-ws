from flask_restx import Model, fields

from seta_flask_server.infrastructure.constants import AuthorizedArea

token_model = Model(
    "Token",
    {
        "token": fields.String(description="Encoded token", required=True),
        "authorizationAreas": fields.List(
            fields.String(enum=AuthorizedArea.List),
            description="List of authorized areas",
        ),
    },
)

ns_models = {token_model.name: token_model}
