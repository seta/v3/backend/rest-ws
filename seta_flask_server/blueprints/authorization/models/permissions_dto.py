from flask_restx import Model, fields
from seta_flask_server.repository.models import DiscoveryDomainEnum
from flask_restx.reqparse import RequestParser

data_source_scope_model = Model(
    "DataSourceScope",
    {
        "id": fields.String(description="Data source identifier.", attribute="id"),
        "index": fields.String(description="Search index name.", attribute="index"),
        "domains": fields.List(
            fields.String(
                enum=[
                    DiscoveryDomainEnum.SEARCH,
                    DiscoveryDomainEnum.RAG,
                    DiscoveryDomainEnum.NLP,
                ]
            ),
            description="Discovery domains",
        ),
    },
)

data_source_scopes_model = Model(
    "DataSourceScopes",
    {
        "ownership": fields.List(fields.Nested(data_source_scope_model)),
        "view": fields.List(fields.Nested(data_source_scope_model)),
        "administrator": fields.List(fields.Nested(data_source_scope_model)),
    },
)

permissions_model = Model(
    "DataSourcePermissions",
    {"permissions": fields.Nested(data_source_scopes_model)},
)

ns_models = {
    data_source_scope_model.name: data_source_scope_model,
    data_source_scopes_model.name: data_source_scopes_model,
    permissions_model.name: permissions_model,
}


domain_parser = RequestParser()
domain_parser.add_argument(
    "domain",
    location="args",
    required=False,
    case_sensitive=False,
    choices=[
        DiscoveryDomainEnum.SEARCH,
        DiscoveryDomainEnum.RAG,
        DiscoveryDomainEnum.NLP,
    ],
    help="Discovery domain",
)
