from flask import current_app
from flask_injector import FlaskInjector

from seta_flask_server.repository.models import DataSourceScopeEnum, DiscoveryDomainEnum
from seta_flask_server.repository.interfaces import (
    IDataSourcesBroker,
    IDataSourceScopesBroker,
    IUserProfileUnsearchables,
    IAppsBroker,
)

from seta_flask_server.infrastructure.constants import UserType


def get_data_source_permissions(
    user_id: str, user_type: str, discovery_domain: str = None
) -> dict:
    """Build data sources permissions."""

    permissions = {"ownership": [], "view": []}

    app_injector: FlaskInjector = current_app.injector
    data_sources_broker = app_injector.injector.get(IDataSourcesBroker)
    scopes_broker = app_injector.injector.get(IDataSourceScopesBroker)
    profile_broker = app_injector.injector.get(IUserProfileUnsearchables)
    apps_broker = app_injector.injector.get(IAppsBroker)

    if user_type == UserType.Application:
        # load parent permissions
        application = apps_broker.get_by_user_id(user_id)

        if application is None:
            return permissions

        user_id = application.parent_user_id

    current_app.logger.debug(
        f"Getting data source permissions for user {user_id}, domain {discovery_domain}"
    )
    data_sources = data_sources_broker.get_all(
        active_only=True, discovery_domain=discovery_domain
    )
    user_scopes = scopes_broker.get_by_user_id(user_id)
    unsearchables = profile_broker.get_unsearchables(user_id)

    for data_source in data_sources:
        data_source_id = data_source.data_source_id

        append_view = True
        discovery_domains = (
            data_source.discovery_domains.copy()
            if data_source.discovery_domains
            else None
        )
        is_unsearchable = unsearchables and any(
            ds_id.lower() == data_source_id for ds_id in unsearchables
        )

        if is_unsearchable:
            if discovery_domains is None:
                append_view = False
            else:
                if DiscoveryDomainEnum.SEARCH in discovery_domains:
                    discovery_domains.remove(DiscoveryDomainEnum.SEARCH)

                # if no discovery domains left, do not append view permission
                if not discovery_domains:
                    append_view = False

        if append_view:
            permissions["view"].append(
                {
                    "id": data_source_id,
                    "index": data_source.index_name,
                    "domains": discovery_domains,
                }
            )

        if any(
            scope.data_source_id == data_source_id
            and scope.scope == DataSourceScopeEnum.DATA_OWNER.value
            for scope in user_scopes
        ):
            permissions["ownership"].append(
                {
                    "id": data_source_id,
                    "index": data_source.index_name,
                    "domains": data_source.discovery_domains,
                }
            )

    return permissions
