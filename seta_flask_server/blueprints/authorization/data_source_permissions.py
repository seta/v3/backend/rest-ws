from http import HTTPStatus
from injector import inject

from flask import abort
from flask_restx import Namespace, Resource

from seta_flask_server.repository import interfaces
from seta_flask_server.infrastructure.constants import UserRoleConstants
from seta_flask_server.blueprints.authorization.models import permissions_dto as dto

from .logic.token_info_logic import get_data_source_permissions

data_sources_ns = Namespace(
    "Data Source Scopes", description="List of data source permissions."
)
data_sources_ns.models.update(dto.ns_models)


@data_sources_ns.route(
    "/<string:user_id>", endpoint="data_source_scopes", methods=["GET"]
)
@data_sources_ns.param("user_id", "User identifier")
class DataSourceScopesResource(Resource):

    @inject
    def __init__(
        self,
        users_broker: interfaces.IUsersBroker,
        *args,
        api=None,
        **kwargs,
    ):

        self.users_broker = users_broker

        super().__init__(api, *args, **kwargs)

    @data_sources_ns.doc(
        description="Get a list of data source permissions for a user",
        responses={
            int(HTTPStatus.OK): "Retrieved data source permissions.",
            int(HTTPStatus.NOT_FOUND): "User id not found.",
        },
    )
    @data_sources_ns.expect(dto.domain_parser)
    @data_sources_ns.marshal_with(dto.permissions_model, mask="*")
    def get(self, user_id: str):
        """Get a list of data source permissions for a user"""

        user = self.users_broker.get_user_by_id(user_id, load_scopes=False)

        if user is None:
            abort(HTTPStatus.NOT_FOUND, "User not found!")

        request_dict = dto.domain_parser.parse_args()
        domain = request_dict.get("domain", None)

        permissions = get_data_source_permissions(user_id, user.user_type, domain)

        if user.role == UserRoleConstants.Admin:
            permissions["administrator"] = [{"id": "all", "index": "all"}]

        return {"permissions": permissions}
