from http import HTTPStatus
from werkzeug.exceptions import HTTPException
from injector import inject

from flask import jsonify, abort, current_app
from flask_jwt_extended import decode_token
from flask_restx import Namespace, Resource


from seta_flask_server.repository import interfaces

from seta_flask_server.infrastructure.constants import (
    AuthorizedArea,
    UserStatusConstants,
)
from seta_flask_server.blueprints.authorization.logic.token_info_logic import (
    get_data_source_permissions,
)

from seta_flask_server.blueprints.authorization.models import token_info_dto as dto

ns_authorization = Namespace("Token Authorization", "Decode JWT token")
ns_authorization.models.update(dto.ns_models)


@ns_authorization.route("", methods=["POST"])
class TokenInfo(Resource):
    @inject
    def __init__(
        self,
        users_broker: interfaces.IUsersBroker,
        session_broker: interfaces.ISessionsBroker,
        *args,
        api=None,
        **kwargs,
    ):
        self.users_broker = users_broker
        self.session_broker = session_broker

        super().__init__(api, *args, **kwargs)

    @ns_authorization.doc(
        description="Returns the decoded token",
        responses={
            int(HTTPStatus.OK): "Decoded token as json",
            int(HTTPStatus.BAD_REQUEST): "No token provided",
            int(HTTPStatus.UNAUTHORIZED): "Unauthorized JWT",
            int(HTTPStatus.UNPROCESSABLE_ENTITY): "Invalid token",
        },
    )
    @ns_authorization.expect(dto.token_model, validate=True)
    def post(self):
        """Decodes the access token"""

        r = ns_authorization.payload

        token = r["token"]
        areas = r.get("authorizationAreas", None)

        decoded_token = None
        try:
            decoded_token = decode_token(encoded_token=token)

            jti = decoded_token.get("jti")
            if jti:
                if self.session_broker.session_token_is_blocked(jti):
                    abort(HTTPStatus.UNAUTHORIZED, "Blocked token")

            # get user permissions for all resources
            seta_id = decoded_token.get("seta_id")
            if seta_id:
                user = self.users_broker.get_user_by_id(
                    seta_id["user_id"], load_scopes=False
                )

                if user is None:
                    abort(HTTPStatus.UNAUTHORIZED, "User not found!")

                if user.status != UserStatusConstants.Active:
                    abort(HTTPStatus.UNAUTHORIZED, "User inactive!")

            if areas is not None and AuthorizedArea.DataSources in areas:

                decoded_token["resource_permissions"] = get_data_source_permissions(
                    user_id=user.user_id, user_type=user.user_type
                )

        except HTTPException:
            raise

        except Exception as e:  # pylint: disable=broad-exception-caught
            message = str(e)
            current_app.logger.exception(message)
            abort(HTTPStatus.UNAUTHORIZED, message)

        current_app.logger.debug(decoded_token)
        return jsonify(decoded_token)
