import os

# Gunicorn config variables
loglevel = os.environ.get("LOG_LEVEL", default="INFO")
errorlog = "-"  # stderr
accesslog = "-"  # stdout
name = "seta-ui"
worker_tmp_dir = "/dev/shm"
graceful_timeout = 0
timeout = 0
keepalive = 30
threads = os.environ.get("THREADS", default=2)
workers = os.environ.get("PROCESSES", default=8)
preload_app = True
