"""Flask migrate application factory"""

import os
from flask import Flask
from sqlalchemy import text
from werkzeug.middleware.proxy_fix import ProxyFix

from seta_flask_server.config import Config

from seta_flask_server.infrastructure.extensions import (
    logs,
    db,
    migrate,
)

#! used by flask-migration
# pylint: disable-next=wildcard-import, unused-wildcard-import
from seta_flask_server.repository.orm_models import *


def create_app(config_object):
    """Web service app factory"""

    flask_app = Flask(__name__)
    # Tell Flask it is Behind a Proxy
    flask_app.wsgi_app = ProxyFix(flask_app.wsgi_app, x_for=1, x_host=1)

    flask_app.config.from_object(config_object)

    # use flask.json in all modules instead of python built-in json
    # app.json_provider_class = MongodbJSONProvider

    register_extensions(flask_app)

    with flask_app.app_context():

        try:
            db.session.execute(text("SELECT 1"))
        except Exception as e:
            flask_app.logger.error("Database connection failed: %s", e)

    return flask_app


def register_extensions(flask_app: Flask):
    """Register application extensions"""

    db.init_app(flask_app)
    migrate.init_app(flask_app, db)

    try:
        logs.init_app(flask_app)
    except Exception as e:
        flask_app.logger.error("logs config failed", e)


stage = os.environ.get("STAGE", default="Production")

configuration = Config(stage)
app = create_app(configuration)
